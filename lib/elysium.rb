require "elysium/engine"

module Elysium
  # Your code goes here...

  module Model 
    def self.included(base)
      base.extend ClassMethods
    end
  end

  module ClassMethods
    def has_elysium
      belongs_to :network, class_name: 'S'
    end
  end
end
