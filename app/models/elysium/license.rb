module Elysium
  class License < ApplicationRecord
    self.table_name = 'licenses'

    belongs_to :network, class_name: 'Signage::Network'

    after_create :read_license_file

    def license_data
      data = crypt.decrypt_and_verify(license_key)
      return JSON.parse(data)
    rescue=>e 
      raise "Unexpected error, you\'ve tried to install unauthorize license file."
    end

    def data
      @data ||= license_data
    end

    def license_valid?
      Date.today <= Date.parse(data['valid_until'])
    end

    def self.fingerprint(network)
      Base64.encode64(Base64.encode64("#{network.id}-#{Rails.application.secrets.secret_key_base[0..31]}-#{Rails.application.config.machine_id.strip}"))
    end

    private

    def read_license_file
      if data.present? and data['provider'] == 'Touchmedia Pte. Ltd.' and network_id == data['network_id'] and data['machine_id'] == Rails.application.config.machine_id.strip
        self.attributes = data
        self.save!
      else
        raise 'Invalid license file.'
      end
    end

    def encryption_key 
      Rails.application.secrets.secret_key_base[0..31]
    end

    def crypt 
      ActiveSupport::MessageEncryptor.new(encryption_key)
    end
  end
end